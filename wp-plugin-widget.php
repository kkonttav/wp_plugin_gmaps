<?php
class Wp_plugin_widget extends WP_Widget {
    public function __construct() {
        parent::__construct(
                'wp_plugin_widget',
                __('Full address',PLUGIN_GMAPS_NAME),
                array('desription' =>__('Displays full given map address',PLUGIN_GMAPS_NAME),)
                );
    }
    
    public function widget($args, $instance) {
        global $wpdb;
        
        $map=$wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "map");
        
        if($map!=null) {
            $latitude=$map->latitude;
            $longitude=$map->longitude;
            $address=$map->address;
            $zip=$map->zip;
            $city=$map->city;
            }
        else {
            $latitude="";
            $longitude="";
            $address="";
            $zip="";
            $city="";
        }
        
        print "<aside class='widget'>";
        print "<p>" 
              . __('Address:',PLUGIN_GMAPS_NAME) 
              . "<br />" 
              . $address
              . "<br />" 
              . $zip 
              . "<br />" 
              . $city
              . "<br />"
              . "Latitude:"
              . "<br />" 
              . $latitude
              . "<br />"
              . "Longitude:"
              . "<br />" 
              . $longitude
              . "<br />"             
              . "</p>";
        print "</aside>";
    }
    
    public function form($instance) {
        // No Widget setting for administrator
    }
    
    public function update($new_instance,$old_instance) {
        // Widget does not have nothing to save
    }
}
