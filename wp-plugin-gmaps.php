<?php
/*
Plugin Name: wp-plugin-gmaps
Plugin URl: http://jee.com
Description: Web arkkitehtuuri kurssin ensimmäinen harjoitustyö Wordpress plugin jossa käyttetään gmaps-laajennusta.
Version: 0.1
Author: Kari Konttavaara
License: GPL v2
*/
?>
<?php
define('PLUGIN_GMAPS_NAME','wp-plugin-gmaps');

include_once('wp-plugin-widget.php');
include_once('wp-plugin-gmaps-class.php');
include_once('wp-plugin-gmaps-tietokannan-luonti.php');

//Add hooks
register_activation_hook(__FILE__,array('Wp_plugin_gmaps_tietokannan_luonti', 'activate'));
register_uninstall_hook(__FILE__,array('Wp_plugin_gmaps_tietokannan_luonti', 'uninstall'));
