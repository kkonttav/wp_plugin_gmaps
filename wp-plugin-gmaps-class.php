<?php
class Wp_plugin_gmaps_class {

    public function __construct() {
//        load_plugin_textdomain(PLUGIN_GMAPS_NAME, false, basename(dirname( __FILE__ ) ). '/languages');
        
        add_action('init', array($this, 'init'));
        add_action('widgets_init',array($this,'register_widget'));
        add_action('admin_menu', array($this, 'setup_admin_menu'));
        
        add_filter('the_title', array($this,'change_map_form_post_title'),10,2);
        add_filter('wp_title', array($this,'change_map_wp_title'),10,2);
    }
    
    public function init() {
        wp_register_style('wpstyle', plugins_url('css/style.css', __FILE__));
        wp_register_script('wpscript',plugins_url('js/delete.js',__FILE__));
        wp_register_script('Googlemaps','http://maps.google.com/maps/api/js');
        wp_register_script('wp_gmaps',plugins_url('js/gmaps.js',__FILE__));        
        add_shortcode('Gmap',array($this,'gmap_shortcode'));
    }
    
    public function setup_admin_menu() {
        add_object_page(PLUGIN_GMAPS_NAME,__('Gmaps',PLUGIN_GMAPS_NAME),'manage_options', PLUGIN_GMAPS_NAME, array($this, 'admin_page'));
        add_submenu_page(PLUGIN_GMAPS_NAME,__('Map', PLUGIN_GMAPS_NAME),__('Map',PLUGIN_GMAPS_NAME),'manage_options',PLUGIN_GMAPS_NAME,array($this,'admin_page'));
        add_submenu_page(PLUGIN_GMAPS_NAME,__('Save location marker',PLUGIN_GMAPS_NAME),__('Save location marker',PLUGIN_GMAPS_NAME),'manage_options','submenu',array($this,'admin_save_location'));
        add_submenu_page(PLUGIN_GMAPS_NAME,__('Saved location markers',PLUGIN_GMAPS_NAME),__('Saved location markers',PLUGIN_GMAPS_NAME),'manage_options','submenu2',array($this,'admin_saved_locations'));
    }
    
    public function register_widget() {
        register_widget('Wp_plugin_widget');
    }

    public function gmap_shortcode(){
        include_once(plugin_dir_path(__FILE__) . 'wp-plugin-show-map.php');
    }

    public function admin_page() {
        include_once(plugin_dir_path(__FILE__) . 'admin/wp-plugin-map.php');
    }    
    
    public function admin_save_location() {
        include_once(plugin_dir_path(__FILE__) . 'admin/wp-save-location.php');
    }

    public function admin_saved_locations() {
        include_once(plugin_dir_path(__FILE__) . 'admin/wp-saved-locations.php');
    }
    
    public function change_map_form_post_title($title,$id) {
        if($title=='wp-plugin-gmaps') {
            global $wpdb;
            $table_name=$wpdb->prefix . "map";
            
            $map=$wpdb->get_row("SELECT * FROM " . $table_name);
            if($map==null) {
                return $title;
            }
            else {
                return $map->city;
            }
        }
        else {
            return $title;
        }
    }
    
    public function change_map_wp_title($title,$id) {
        if(substr($title,0,15)=='wp-plugin-gmaps') {
            global $wpdb;

            $table_name=$wpdb->prefix . "map";

            $map=$wpdb->get_row("SELECT * FROM " . $table_name);
            if($map==null) {
                return $title;
            }
            else {
                return $map->city . " | ";
            }
        }
        else {
            return $title;
        }
    }
    
}

add_action('plugins_loaded','wp_plugin_gmaps_init');

function wp_plugin_gmaps_init() {
    $wp_plugin_gmaps = new Wp_plugin_gmaps_class();
}