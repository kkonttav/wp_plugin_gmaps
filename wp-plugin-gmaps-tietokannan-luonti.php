<?php
class Wp_plugin_gmaps_tietokannan_luonti {

public static function activate() {
    global $wpdb;
    $table_name1=$wpdb->prefix . "map";
    
    $sql = "CREATE TABLE IF NOT EXISTS $table_name1 (
            id int PRIMARY KEY AUTO_INCREMENT,
            latitude varchar(9) NOT NULL,
            longitude varchar(9) NOT NULL,
            address varchar(50) NOT NULL,
            zip varchar(15) NOT NULL,
            city varchar(50) NOT NULL
            );";
            
    require_once(ABSPATH . "wp-admin/includes/upgrade.php");
    dbDelta($sql);

    $table_name2=$wpdb->prefix . "marker";
    
    $sql = "CREATE TABLE IF NOT EXISTS $table_name2 (
            id int PRIMARY KEY AUTO_INCREMENT,
            description varchar(50) NOT NULL,
            latitude varchar(9) NOT NULL,
            longitude varchar(9) NOT NULL
            );";
    
    require_once(ABSPATH . "wp-admin/includes/upgrade.php");
    dbDelta($sql);
    
    add_option("wp-plugin-gmaps-db-version","0.1");
    }

public static function uninstall() {
    global $wpdb;
    $table_name  =$wpdb->prefix . "map";
    $sql = "DROP TABLE IF EXISTS $table_name;";
    $wpdb->query($sql);
    $table_name = $wpdb->prefix . "marker";
    $sql = "DROP TABLE IF EXISTS $table_name;";
    $wpdb->query($sql);
    delete_option("wp-plugin-gmaps-db-version");
    }

}