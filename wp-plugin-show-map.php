<?php
wp_enqueue_style('wpstyle');
wp_enqueue_script('Googlemaps');
wp_enqueue_script('wp_gmaps');

print "<h2>";
_e('Map from given coordinates.',PLUGIN_GMAPS_NAME);
print "</h2>";

global $wpdb;

/* Default location 'Oulu' */
$latitude=65.006702;
$longitude=25.490495;

$table_name=$wpdb->prefix . "map";
$map=$wpdb->get_row("SELECT * FROM " . $table_name);
if($map!=null) {
    $latitude=$map->latitude;
    $longitude=$map->longitude;
}

$table_name=$wpdb->prefix . "marker";
$markers=$wpdb->get_results("SELECT * FROM " . $table_name);

if($markers!=null) {
    foreach ($markers as $marker) {
        print "<br />";
        print $marker->description;
        print "<br />";        
        print $marker->latitude;
        print "<br />";
        print $marker->longitude;
        print "<br />";
    }
}    
?>
<script>
    var map;    
    $(document).ready(function(){      
        map = new GMaps({
            div: '#map',                
            lat: <?php print($latitude);?>,
            lng: <?php print($longitude);?>
        });

//    map.addMarker({
//            lat: ,
//            lng: ,
//            title: 
//          });
            
    });
</script> 
