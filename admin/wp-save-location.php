<?php
wp_enqueue_style('wpstyle');

if($_SERVER["REQUEST_METHOD"] == "POST") {
?>
<div class="updated">
    <p>
    <?php
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $description = sanitize_text_field($_POST["description"]);
    
    global $wpdb;
    $table_name=$wpdb->prefix . "marker";    
    $wpdb->insert (
        $table_name,
        array(
            'description' => $description,
            'latitude' => $latitude,
            'longitude' => $longitude
            )
        );
    _e('Location saved.',PLUGIN_GMAPS_NAME);
?>
    </p>
</div>
<?php
}
?>
<div class="entry_content">
    <h2><?php _e('Save location marker', PLUGIN_GMAPS_NAME); ?></h2>
    <form method="post" action="">
        <label for="latitude"><?php _e('latitude',PLUGIN_GMAPS_NAME); ?>:</label>
        <input id="latitude" name='latitude' size="9" maxlength="9" autofocus>

        <label for="longitude"><?php _e('longitude',PLUGIN_GMAPS_NAME); ?>:</label>
        <input id="longitude" name='longitude' size="9" maxlength="9">

        <label for="description"><?php _e('Description',PLUGIN_GMAPS_NAME); ?>:</label>
        <textarea id="description" name='description'></textarea>

        <div class="buttons">        
            <input type='submit' class='button button-primary' value='<?php _e('Save', PLUGIN_GMAPS_NAME) ?>'>
        </div>
    </form>
</div>
