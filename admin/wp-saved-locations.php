<?php
wp_enqueue_script('wpscript');

if (isset($_POST["id"])) {
    global $wpdb;
    $table_name=$wpdb->prefix . "marker";
    $idt=$_POST["id"];
    if(isset($_POST["action"])) {
        foreach ($idt as $id) {
            $id=  intval($id);
            $temp=array("id" => $id);
            $wpdb->delete($table_name, $temp);
        }
        print "<div class='updated'><p>";
        _e('Selected markers deleted!',PLUGIN_GMAPS_NAME);
        print "</p></div>";
    }
}
?>
<div class="wrap">
    <h2><?php _e('Saved locations',PLUGIN_GMAPS_NAME);?></h2>
    <form method="post" action="">
        <div class="tablenav top">
            <div class="alignleft action">
                <select name="action">
                    <option value="2"><?php _e('Delete',PLUGIN_GMAPS_NAME);?></option>
                </select>
                <input type="submit" value="<?php _e('Accept',PLUGIN_GMAPS_NAME); ?>" class="button action">
            </div>
        </div>
    <table class="wp-list-table widefat">
        <thead>
            <tr>
                <th class="mange-column check-column"></th>
                <th class="column-title"><?php _e('Latidude',PLUGIN_GMAPS_NAME); ?></th>
                <th><?php _e('Longitude',PLUGIN_GMAPS_NAME); ?></th>
                <th><?php _e('Description',PLUGIN_GMAPS_NAME); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            global $wpdb;
            $table_name=$wpdb->prefix . "marker";
            $sql="SELECT * FROM " . $table_name . " ORDER BY id";
            $locations=$wpdb->get_results($sql);
            
            if($locations) {
                foreach ( $locations as $location) {
                    print "<tr>";
                    print "<td><input type='checkbox' name='id[]' value='" . $location->id . "'></td>";
                    print "<td>" . sanitize_text_field($location->latitude);
                    print "<div class='row-actions'>";
                        print "<span class='trash'><a href='#' id='$location->id' class='submitdelete'>" . __('Delete',PLUGIN_GMAPS_NAME) . "</a></span>";
                    print "</div>";
                    print "</td>";
                    print "<td>" . sanitize_text_field($location->longitude) . "</td>";
                    print "<td>" . sanitize_text_field($location->description) . "</td>";
                    print "</tr>";
                }
            }
            ?>        
        </tbody>
    </table>
        <div class="tablenav bottom">
            <div class="alignleft action">
                <select name="action">
                    <option value="2"><?php _e('Delete',PLUGIN_GMAPS_NAME);?></option>
                </select>
                <input type="submit" value="<?php _e('Accept', PLUGIN_GMAPS_NAME);?>" class="button action">
            </div>
        </div>
    </form>
</div>