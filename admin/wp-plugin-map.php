<?php
wp_enqueue_style('wpstyle');
/* KaKo lisäykset / siirrot */
global $wpdb;
$table_name=$wpdb->prefix . "map";
/* KaKo lisäykset / siirrot */
if (isset($_POST["latitude"])) {
?>
<div class="updated">
    <p>
    <?php
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $address = sanitize_text_field($_POST["address"]);
    $zip = sanitize_text_field($_POST["zip"]);
    $city = sanitize_text_field($_POST["city"]);
    $map = $wpdb->get_row("SELECT * FROM ". $table_name);
    if($map==null) {
        $wpdb->insert (
            $table_name,
            array(
                'latitude' => $latitude,
                'longitude' => $longitude,
                'address' => $address,
                'zip' => $zip,
                'city' => $city
                )
            );
    }
    else {
        $id=$map->id;
        $wpdb->update (
                $table_name,
                array(
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'address' => $address,
                    'zip' => $zip,
                    'city' => $city
                    ),
                array('id'=>$id)
            );
    }
    _e('Map location saved.',PLUGIN_GMAPS_NAME);
?>
    </p>
</div>
<?php
}
else {
    $map=$wpdb->get_row("SELECT * FROM " . $table_name);
    if($map!=null) {
        $latitude=$map->latitude;
        $longitude=$map->longitude;
        $address=$map->address;
        $zip=$map->zip;
        $city=$map->city;
        }
    else {
        $latitude="";
        $longitude="";
        $address="";
        $zip="";
        $city="";
        }
}
?>
<div class="entry_content">
    <h2><?php _e('Save map location', PLUGIN_GMAPS_NAME); ?></h2>
    <form method="post" action="">
        <label for="latitude"><?php _e('latitude',PLUGIN_GMAPS_NAME); ?>:</label>
        <input id="latitude" name='latitude' size="9" maxlength="9" value="<?php print($latitude);?>" autofocus>
        
        <label for="longitude"><?php _e('longitude',PLUGIN_GMAPS_NAME); ?>:</label>
        <input id="longitude" name='longitude' size="9" maxlength="9" value="<?php print($longitude);?>" >

        <label for="address"><?php _e('address',PLUGIN_GMAPS_NAME); ?>:</label>
        <input id="address" name='address' size="50" maxlength="50" value="<?php print($address);?>" >

        <label for="zip"><?php _e('zip',PLUGIN_GMAPS_NAME); ?>:</label>
        <input id="zip" name='zip' size="15" maxlength="15" value="<?php print($zip);?>" >
        
        <label for="city"><?php _e('city',PLUGIN_GMAPS_NAME); ?>:</label>
        <input id="city" name='city' size="50" maxlength="50" value="<?php print($city);?>" >
        <div class="buttons">
            <input type='submit' class='button button-primary' value='<?php _e('Save', PLUGIN_GMAPS_NAME) ?>'>
        </div>
    </form>
</div>
